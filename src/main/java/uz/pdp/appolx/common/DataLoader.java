package uz.pdp.appolx.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.appolx.entity.*;
import uz.pdp.appolx.repository.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.sql.init.mode}")
    String initMode;

    final TariffRepository tariffRepository;
    final RoleRepository roleRepository;
    final RegionRepository regionRepository;
    final AttachmentRepository attachmentRepository;
    final AdvertisementRepository advertisementRepository;
    final CategoryRepository categoryRepository;
    final DistrictRepository districtRepository;
    final UserRepository userRepository;
    final PasswordEncoder passwordEncoder;

    public DataLoader(RoleRepository roleRepository, RegionRepository regionRepository, AttachmentRepository attachmentRepository, AdvertisementRepository advertisementRepository, CategoryRepository categoryRepository,
                      DistrictRepository districtRepository, UserRepository userRepository, TariffRepository tariffRepository, PasswordEncoder passwordEncoder) {
        this.roleRepository = roleRepository;
        this.regionRepository = regionRepository;
        this.attachmentRepository = attachmentRepository;
        this.advertisementRepository = advertisementRepository;
        this.categoryRepository = categoryRepository;
        this.districtRepository = districtRepository;
        this.userRepository = userRepository;
        this.tariffRepository = tariffRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {

            //SAVING ROLES
            Role saveRoleAdmin = roleRepository.save(new Role("ROLE_ADMIN", null));
            Role saveRoleUser = roleRepository.save(new Role("ROLE_USER", null));

            Set<Role> rolesAdmin = new HashSet<>();
            rolesAdmin.add(saveRoleAdmin);

            Set<Role> rolesUser = new HashSet<>();
            rolesUser.add(saveRoleUser);

            //SAVING USERS
            User jonyUser = new User(
                    "Abdulaziz Zarifboyev",
                    "abdulaziz@mail.com",
                     passwordEncoder.encode("1"),
                    "+998998887688",
                    450000.0,
                    rolesUser);

            User nodirbekAdmin = new User(
                    "Nodirbek Nurqulov",
                    "nodirbek@mail.com",
                     passwordEncoder.encode("2"),
                    "+998998788438",
                    400000.0,
                    rolesAdmin);

            userRepository.save(jonyUser);
            userRepository.save(nodirbekAdmin);

            //SAVING TARIFFS
            Tariff start = new Tariff("Start", 3);
            Tariff tezkor = new Tariff("Tezkor", 7);
            Tariff turbo = new Tariff("Turbo", 30);
            tariffRepository.save(start);
            tariffRepository.save(tezkor);
            tariffRepository.save(turbo);

            //SAVING REGIONS AND DISTRICTS
            Region tashkent = new Region("Tashkent");
            Region Sirdaryo = new Region("Sirdaryo");
            Region Karakalpakistan = new Region("Karakalpakistan");
            Region Kashkadarya = new Region("Kashkadarya");
            Region Surkxandarya = new Region("Surkxandarya");
            Region djizzax = new Region("Djizzax");
            Region andijan = new Region("Andijan");
            Region fergana = new Region("Fergana");
            Region namangan = new Region("Namangan");
            Region samarkand = new Region("Samarkand");
            regionRepository.save(tashkent);
            regionRepository.save(Sirdaryo);
            regionRepository.save(Karakalpakistan);
            regionRepository.save(Kashkadarya);
            regionRepository.save(Surkxandarya);
            regionRepository.save(djizzax);
            regionRepository.save(andijan);
            regionRepository.save(fergana);
            regionRepository.save(namangan);
            regionRepository.save(samarkand);


            District shayhantahur = new District("Shayhantahur", tashkent);
            District boyovut = new District("Boyovut", Sirdaryo);
            District beruniy = new District("Beruniy", Karakalpakistan);
            District koson = new District("Koson", Kashkadarya);
            District zomin = new District("Zomin", djizzax);
            District asaka = new District("Asaka", andijan);
            District beshqariq = new District("Beshqariq", fergana);
            District pop = new District("Pop", namangan);
            District urgut = new District("Urgut", samarkand);
            districtRepository.save(shayhantahur);
            districtRepository.save(boyovut);
            districtRepository.save(beruniy);
            districtRepository.save(koson);
            districtRepository.save(zomin);
            districtRepository.save(asaka);
            districtRepository.save(beshqariq);
            districtRepository.save(pop);
            districtRepository.save(urgut);

            //ATTACHMENT SAVING
            Attachment iphone12 = new Attachment(
                    "image/png",
                    "jdshdjhdj",
                    12000,
                    "image1",
                    null);

            Attachment iphone13 = new Attachment(
                    "image",
                    "mnnmn",
                    1000,
                    "image/png",
                    null);

            List<Attachment> attachments = new ArrayList<>();
            attachments.add(iphone12);
            attachments.add(iphone13);
            List<Attachment> savedAdvertisementAttachments = attachmentRepository.saveAll(attachments);

            //SAVING CATEGORY
            Category electronics = new Category(
                    "Electronics",
                    true,
                    null);
            Category telephones = new Category(
                    "Telephones",
                    true,
                    1);
            Category computers = new Category(
                    "Computers",
                    true,
                    2);
            Category photos = new Category(
                    "Photos",
                    true,
                    3);
            categoryRepository.save(electronics);
            Category savedTelephone = categoryRepository.save(telephones);
            categoryRepository.save(computers);
            categoryRepository.save(photos);

            //SAVED ADVERTISEMENT
            Advertisement savedAdvertisement = new Advertisement(
                    "ReadMe 7 ikki yil ishlatilgan",
                     savedTelephone,
                     savedAdvertisementAttachments,
                    "Korishib gaplashamiz oladigan bolsangiz telephone qiling! Tel qilish vaqti 14:00 PM dan 15:00 gacha",
                    1000000.0,
                    false
            );
            advertisementRepository.save(savedAdvertisement);

        }
    }
}
