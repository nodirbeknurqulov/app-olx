package uz.pdp.appolx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appolx.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
