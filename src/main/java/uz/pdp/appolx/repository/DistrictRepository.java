package uz.pdp.appolx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appolx.entity.District;

public interface DistrictRepository extends JpaRepository<District, Integer> {

}
