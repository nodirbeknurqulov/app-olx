package uz.pdp.appolx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appolx.entity.Tariff;

public interface TariffRepository extends JpaRepository<Tariff,Integer> {

}

