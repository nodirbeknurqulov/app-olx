package uz.pdp.appolx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appolx.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {


}
