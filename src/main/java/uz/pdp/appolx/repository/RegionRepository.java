package uz.pdp.appolx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appolx.entity.Region;

public interface RegionRepository extends JpaRepository<Region, Integer> {

}
