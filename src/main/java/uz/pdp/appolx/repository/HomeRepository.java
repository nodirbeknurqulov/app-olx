package uz.pdp.appolx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appolx.entity.Home;

public interface HomeRepository extends JpaRepository<Home,Integer> {
}
