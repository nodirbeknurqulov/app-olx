package uz.pdp.appolx.repository;
// Nurkulov Nodirbek 4/13/2022  6:42 PM

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appolx.entity.Advertisement;

public interface AdvertisementRepository extends JpaRepository<Advertisement,Integer>{
}
