package uz.pdp.appolx.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.pdp.appolx.entity.templates.AbsEntity;

import javax.persistence.Entity;

// Nurkulov Nodirbek 4/13/2022  6:35 PM
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "categories")
public class Category extends AbsEntity {
    private String name;

    private boolean isEnable;

    private Integer parentId;
}
