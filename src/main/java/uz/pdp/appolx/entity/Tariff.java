package uz.pdp.appolx.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.pdp.appolx.entity.templates.AbsEntity;

import javax.persistence.Entity;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "tariffs")
public class Tariff extends AbsEntity {

    private String name;

    private int days;

//    public Tariff(String name, int days) {
//        this.name = name;
//        this.days = days;
//    }
}