package uz.pdp.appolx.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.pdp.appolx.entity.templates.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "attachment_contents")
public class AttachmentContent extends AbsEntity {

    byte[] data;

    @OneToOne(cascade = CascadeType.MERGE)
    private Attachment attachment;
}