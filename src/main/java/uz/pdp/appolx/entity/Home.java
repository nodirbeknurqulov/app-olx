package uz.pdp.appolx.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.PackagePrivate;
import uz.pdp.appolx.entity.templates.AbsEntity;

import javax.persistence.Entity;

// Nurkulov Nodirbek 4/13/2022  6:54 PM
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "homes")
@PackagePrivate
public class Home extends AbsEntity {
    String name;
}
