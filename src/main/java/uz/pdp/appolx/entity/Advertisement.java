package uz.pdp.appolx.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.pdp.appolx.entity.templates.AbsEntity;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "advertisements")
public class Advertisement extends AbsEntity {

    @Column(nullable = false)
    private String shortDescription;

    @ManyToOne
    private Category category;

    @OneToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "advertisements_attachments",
    joinColumns = {@JoinColumn(name = "advertisement_id")},
    inverseJoinColumns = {@JoinColumn(name = "attachment_id")})
    private List<Attachment> attachments;

    private String longDescription;

    private Double initialPrice;

    @OneToOne
    private Tariff tariff;

    private boolean isNew = true;

    @ManyToOne
    private User user;

    private boolean isActive;

    public Advertisement(String shortDescription, Category category, List<Attachment> attachments, String longDescription, Double initialPrice, boolean isNew) {
        this.shortDescription = shortDescription;
        this.category = category;
        this.attachments = attachments;
        this.longDescription = longDescription;
        this.initialPrice = initialPrice;
        this.isNew = isNew;
    }
}
