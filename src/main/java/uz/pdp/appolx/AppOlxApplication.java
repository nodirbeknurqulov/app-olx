package uz.pdp.appolx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppOlxApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppOlxApplication.class, args);
    }

}
