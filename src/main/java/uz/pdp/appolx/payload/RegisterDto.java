package uz.pdp.appolx.payload;
// Nurkulov Nodirbek 4/8/2022  11:20 AM

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RegisterDto {

    @NotNull
    private String fullName;

    @NotNull
    private String email;

    @NotNull
    private String password;
}
