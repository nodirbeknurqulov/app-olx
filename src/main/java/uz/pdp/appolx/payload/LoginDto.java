package uz.pdp.appolx.payload;
// Nurkulov Nodirbek 4/8/2022  11:20 AM

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginDto {

    @NotNull
    private String email;

    @NotNull
    private String password;
}
