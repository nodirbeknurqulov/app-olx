package uz.pdp.appolx.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uz.pdp.appolx.entity.User;
import uz.pdp.appolx.repository.UserRepository;

import java.util.Collections;
import java.util.Optional;

// Nurkulov Nodirbek 4/8/2022  10:29 AM
@Service
@RequiredArgsConstructor
public class UserService{

    private final UserRepository userRepository;

//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        Optional<User> optionalUser = userRepository.findByUsername(username);
//        if (!optionalUser.isPresent()) {
//            throw new UsernameNotFoundException(username);
//        }
//        User user = optionalUser.get();
//        return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(), Collections.emptyList());
//    }
}
