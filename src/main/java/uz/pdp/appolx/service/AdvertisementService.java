package uz.pdp.appolx.service;
// Nurkulov Nodirbek 4/14/2022  8:30 AM

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.appolx.entity.Advertisement;
import uz.pdp.appolx.payload.ApiResponse;
import uz.pdp.appolx.repository.AdvertisementRepository;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AdvertisementService {

    private final AdvertisementRepository advertisementRepository;

    public List<Advertisement> getAllAdvertisements(int page, int size) {
        PageRequest pagesAdvertisements = PageRequest.of(page, size);
        Page<Advertisement> advertisementPage = advertisementRepository.findAll(pagesAdvertisements);
        return advertisementPage.getContent();
    }

    public ApiResponse getAdvertisementById(Integer id) {
        Optional<Advertisement> optionalAdvertisement = advertisementRepository.findById(id);
        return optionalAdvertisement.map(actor -> new ApiResponse("Success", true, actor))
                .orElseGet(() -> new ApiResponse("Advertisement  not found!", false));
    }

    public ApiResponse addActor(Advertisement advertisement, List<MultipartFile> files) throws IOException {

//        Attachment attachment = attachmentService.uploadFile(file);
//        if (attachment == null) {
//            return new ApiResponse("Error!", false);
//        }
//
//        Actor newActor = new Actor();
//        newActor.setAttachment(attachment);
//        newActor.setBio(actor.getBio());
//        newActor.setFullName(actor.getFullName());
//        Actor save = actorRepo.save(newActor);
//        return new ApiResponse("Actor added!!!", true, save);
        return null;
    }


    public ApiResponse updateActor(Integer id, Advertisement advertisement, MultipartFile file) throws IOException {
//        Optional<Actor> optionalActor = actorRepo.findById(id);
//        if (!optionalActor.isPresent()) {
//            return new ApiResponse("Actor not found!", false);
//        }
//        Actor updatingActor = optionalActor.get();
//        updatingActor.setFullName(actor.getFullName());
//        updatingActor.setBio(actor.getBio());
//
//        Attachment attachment = attachmentService.uploadFile(file);
//        updatingActor.setAttachment(attachment);
//        Actor savedActor = actorRepo.save(updatingActor);
//        return new ApiResponse("Actor updated!", true,savedActor);
        return null;
    }


    public ApiResponse deleteActor(Integer id) {
//        Optional<Actor> optionalActor = actorRepo.findById(id);
//        if (!optionalActor.isPresent()){
//            return new ApiResponse("Actor not found!!!", false);
//        }
//        Actor actor = optionalActor.get();
//        actorRepo.delete(actor);
//        return new ApiResponse("Actor deleted!!!", true);
        return null;
    }
}