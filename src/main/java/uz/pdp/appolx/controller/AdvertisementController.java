package uz.pdp.appolx.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.appolx.entity.Advertisement;
import uz.pdp.appolx.payload.ApiResponse;
import uz.pdp.appolx.service.AdvertisementService;

import java.io.IOException;
import java.util.List;

// Nurkulov Nodirbek 4/14/2022  8:28 AM
@RestController
@RequestMapping("/api/advertisement")
@RequiredArgsConstructor
public class AdvertisementController {

    private final AdvertisementService advertisementService;

    @PreAuthorize(value = "hasAuthority('GET_ALL_ADVERTISEMENT')")
    @GetMapping
    public HttpEntity<?> getAllAdvertisements(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size) {
        List<Advertisement> allActors = advertisementService.getAllAdvertisements(page, size);
        return ResponseEntity.ok(allActors);
    }

    @PreAuthorize(value = "hasAuthority('GET_ONE_ADVERTISEMENT')")
    @GetMapping("/{id}")
    public HttpEntity<?> getAdvertisement(@PathVariable Integer id){
        ApiResponse apiResponse = advertisementService.getAdvertisementById(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:404).body(apiResponse);
    }

    @PreAuthorize(value = "hasAuthority('ADD_ADVERTISEMENT')")
    @PostMapping
    public HttpEntity<?> addAdvertisement(@RequestPart(name = "json") Advertisement advertisement,
                                  @RequestPart(name = "files") MultipartFile request) throws IOException {
// TODO: 4/14/2022
//        ApiResponse apiResponse = actorService.addActor(actor, request);
//        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
        return null;
    }

    @PreAuthorize(value = "hasAuthority('EDIT_ADVERTISEMENT')")
    @PutMapping("/{id}")
    public ApiResponse updateActor(@PathVariable Integer id,
                                   @RequestPart(name = "json") Advertisement actor,
                                   @RequestPart(name = "file") MultipartFile file) throws IOException {
// TODO: 4/14/2022  
        //        return actorService.updateActor(id, actor, file);
        return null;
    }
    
    @PreAuthorize(value = "hasAuthority('DELETE_ADVERTISEMENT')")
    @DeleteMapping("/{id}")
    public ApiResponse deleteActor(@PathVariable Integer id) {
        // TODO: 4/14/2022
//        return actorService.deleteActor(id);
        return null;
    }
}
